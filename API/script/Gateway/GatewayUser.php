<?php

namespace Gateway;

use Config\ConnectClass;
use Config\Connection;
use PDO;
use PDOException;

class GatewayUser
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getUserPassword(string $login): array
    {
        $query = "SELECT id,password FROM `user` WHERE login = :login";
        $this->connection->executeQuery($query, array(
            ':login' => array($login, PDO::PARAM_STR)
        ));
        $result = $this->connection->getResults();
        if(empty($result))
            return array(
                "id"=>-1,
                0 => -1,
                "password" => "",
                1 => ""
            );
        return $result[0];
    }


    public function addUser(String $login, String $password): void
    {
        $query = "INSERT INTO `user`(login,password) VALUES(:login, :password)";
        $this->connection->executeQuery($query, array(
            ':login' => array($login, PDO::PARAM_STR),
            ':password' => array($password, PDO::PARAM_STR)
        ));
    }

    public function getAllUsers(): ?array
    {
        $query = "SELECT login FROM `user`";
        $this->connection->executeQuery($query);
        $result = $this->connection->getResults();
        if(empty($result))
            return null;
        return $result;
    }
}