<?php

namespace Gateway;

use Config\ConnectClass;
use Config\Connection;
use PDO;
use PDOException;

class GatewaySouvenir
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet de récupérer le mot de passe de l'administrateur en fonction de son login.
     * @param int $id
     * @return array Le souvenir de l'utilisateur sélectionné
     */

    public function getSouvenirForUser(int $id): array
    {
        $query = "SELECT * FROM `souvenir` WHERE userId = :userId";
        $this->connection->executeQuery($query, array(
            ':userId' => array($id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function addSouvenir(String $title, String $linkImage, String $description, float $longitude, float $latitude, float $altitude, int $userId): void
    {
        $query = "INSERT INTO `souvenir`(title, linkImage, description, longitude, latitude, altitude, userId) VALUES
                                        (:title, :linkImage, :description, :longitude, :latitude, :altitude, :userId)";
        $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR),
            ':linkImage' => array($linkImage, PDO::PARAM_STR),
            ':description' => array($description, PDO::PARAM_STR),
            ':longitude' => array($longitude, PDO::PARAM_STR),
            ':latitude' => array($latitude, PDO::PARAM_STR),
            ':altitude' => array($altitude, PDO::PARAM_STR),
            ':userId' => array($userId, PDO::PARAM_INT)
        ));
    }

    public function deleteSouvenir(int $id): void
    {
        $query = "DELETE FROM `souvenir` WHERE id=:id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }
}
