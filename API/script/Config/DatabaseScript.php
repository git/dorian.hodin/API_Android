<?php

namespace Config;

use PDOException;

class DatabaseScript {

    private Connection $connection;
    public function __construct() {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function executeScript(): void
    {
        $queryScript = '

CREATE TABLE `souvenir` (
  `id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(50) NOT NULL,
  `linkImage` text NOT NULL,
  `description` text NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `altitude` float NOT NULL,
  `userId` int(11) NOT NULL,
  `dossierId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `user` (
  `id` int(11) NOT NULL DEFAULT 1,
  `login` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `dossier` (
  `id` int(11) NOT NULL DEFAULT 1,
  `userId` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `souvenir`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
    
ALTER TABLE `dosser`
  ADD PRIMARY KEY (`id`),

ALTER TABLE `souvenir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `dossier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `souvenir`
  ADD CONSTRAINT `Categorize_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `categorize_ibfk_1` FOREIGN KEY (`dosserId`) REFERENCES `dossier` (`id`),

 ';
        $this->connection->executeQuery($queryScript);
    }
}
