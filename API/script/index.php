<?php

use ExceptionHandle\HttpNotFoundError;
use ExceptionHandle\PDOError;
use ExceptionHandle\TypeErrorParameters;
use Gateway\GatewaySouvenir;
use Gateway\GatewayUser;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require './Config/vendor/autoload.php';

/**
 * Instantiate App
 */
$app = AppFactory::create();

// Add Routing Middleware
$app->addRoutingMiddleware();

/**
 * Add Error Handling Middleware
 *
 * @param bool $displayErrorDetails -> Should be set to false in production
 * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
 * @param bool $logErrorDetails -> Display error details in error log
 */
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

/**
 * Add a route for the API
 */
$app->get('/', function (Request $request) {
    throw new HttpNotFoundError($request);
});

$app->get('/getSouvenirForUser', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewaySouvenir)->getSouvenirForUser($parameters['id']), JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/addSouvenir', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['title'])
        || empty($parameters['linkImage'])
        || empty($parameters['description'])
        || empty($parameters['longitude'])
        || empty($parameters['latitude'])
        || empty($parameters['altitude'])
        || empty($parameters['userId'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewaySouvenir)->addSouvenir($parameters['title'],
                                            $parameters['linkImage'],
                                            $parameters['description'],
                                            $parameters['longitude'],
                                            $parameters['latitude'],
                                            $parameters['altitude'],
                                            $parameters['userId']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->delete('/deleteSouvenir', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewaySouvenir)->deleteSouvenir($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/getAllUsers', function(Request $request, Response $response){
    try{
        $response->getBody()->write(json_encode((new GatewayUser)->getAllUsers(), JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getUserPassword/{login}', function(Request $request, Response $response, array $args){
    if (empty($args['login'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayUser)->getUserPassword($args['login']), JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/addUser/{login}/{password}', function(Request $request, Response $response, array $args){
    if (empty($args['login']) || empty($args['password'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayUser)->addUser($args['login'],$args['password']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});



// Run app
$app->run();
